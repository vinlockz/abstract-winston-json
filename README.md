# Abstract Winston JSON Logging Tool

A Simple JSON Logging Tool that is built on top of Winston

## Installation
Yarn:
```bash
yarn add abstract-winston-json
```
NPM:
```bash
npm install abstract-winston-json
```

## Output

```json
{
  "type": "log.type",
  "message": {
    "data": {
      "more": "data",
    },  
  },
  "metadata": {
    "userId": "123ABC",
  },
}
```

## API

### Logger

#### Parameters

| Param       | Type    | Description     | Default Value |
| ----------- | ------- | --------------- | ------------- |
| metadata    | Object  | Metadata Object | {}            |
| debugMode   | Boolean | Debug Mode      | false         |

#### Example

```js
const Logger = require('abstract-winston-json');

// Metadata to include in every log.
const metadata = {
  userId: 'ABC123',
};

// Debug Mode Off
const logger = new Logger({ metadata });

// Debug Mode On
const logger = new Logger({
  metadata,
  debugMode: true,
});
```

### addMetadata(key, value)

Add Metadata to include in every log.

#### Parameters

| Param       | Type   | Description     | Default Value |
| ----------- | ------ | --------------- | ------------- |
| key         | String | Metadata Key    |               |
| value       | Any    | Metadata Value  |               |

#### Example

```js
logger.addMetadata('key', 'value');
```

### removeMetadata(key)

Remove specific Metadata from logs from now on.

#### Parameters

| Param       | Type   | Description     | Default Value |
| ----------- | ------ | --------------- | ------------- |
| key         | String | Metadata Key    | {}            |

#### Example

```js
logger.removeMetadata('key');
```

### log(type, message)

Log via the `info` level.

#### Parameters

| Param       | Type   | Description     | Default Value |
| ----------- | ------ | --------------- | ------------- |
| type        | String | Log Type Key    |               |
| message     | Any    | Log Message     |               |

#### Example

```js
logger.log('log.type', {
  requestInfo: 'example',
  data: {},
});
```

### warn(type, message)

Log via the `warn` level.

#### Parameters

| Param       | Type   | Description     | Default Value |
| ----------- | ------ | --------------- | ------------- |
| type        | String | Log Type Key    |               |
| message     | Any    | Log Message     |               |

#### Example

```js
logger.warn('warn.type', {
  requestInfo: 'example',
  data: {},
});
```

### error(type, message) || err(type, message)

Log via the `error` level.

#### Parameters

| Param       | Type   | Description     | Default Value |
| ----------- | ------ | --------------- | ------------- |
| type        | String | Log Type Key    |               |
| message     | Any    | Log Message     |               |

#### Example

```js
logger.error('error.type', {
  requestInfo: 'example',
  data: {},
});
```
```js
logger.err('error.type', {
  requestInfo: 'example',
  data: {},
});
```

### debug(type, message)

Will only log when debug mode is set to `true`.
Logs via the `info` level.

#### Parameters

| Param       | Type   | Description     | Default Value |
| ----------- | ------ | --------------- | ------------- |
| type        | String | Log Type Key    |               |
| message     | Any    | Log Message     |               |

#### Example

```js
logger.debug('error.type', {
  requestInfo: 'example',
  data: {},
});
```

### getLogger()

Gets the underlying Winston Logging Instance

### Example

```js
const winston = logger.getLogger();
```