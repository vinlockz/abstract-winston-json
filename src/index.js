const winston = require('winston');
const CircularJSON = require('circular-json');
const CloudWatchTransport = require('winston-aws-cloudwatch');

class Logger {
  constructor({
    metadata = {},
    debugMode = false,
    preFormatters = [],
    postFormatters = [],
    cloudWatchEnabled = false,
    cloudWatchLogGroupName = null,
    cloudWatchLogStreamName = null,
    awsRegion = 'us-west-2',
  }) {
    this.debugMode = debugMode;
    this.metadata = Object.assign({}, metadata);
    this.logFormat = (info) => {
      let { message } = info;
      if (typeof message === 'object') {
        message = CircularJSON.stringify(message);
      }
      return `${info.level}: ${message}`;
    };
    
    const formatters = [
      ...preFormatters,
      winston.format.printf(this.logFormat),
      ...postFormatters,
    ];
    
    this.winstonOptions = {
      level: 'info',
      format: winston.format.combine(...formatters),
      transports: [
        new winston.transports.Console(),
      ],
    };

    this.winston = winston.createLogger(this.winstonOptions);

    if (cloudWatchEnabled) {
      const cloudWatchConfig = {
        logGroupName: cloudWatchLogGroupName,
        logStreamName: cloudWatchLogStreamName,
        createLogGroup: true,
        createLogStream: true,
        formatLog: this.logFormat,
        awsConfig: {
          region: awsRegion,
        },
      };

      this.winston.add(new CloudWatchTransport(cloudWatchConfig));

      // this.winston.stream = {
      //   write: function(message, encoding) {
      //     this.winston.logger.info(message);
      //   },
      // };
    }
  }

  addMetadata = (key, value) => {
    if (typeof key === 'object') {
      this.metadata = Object.assign(this.metadata, key);
    } else {
      this.metadata[String(key)] = value;
    }
    return this;
  };

  removeMetadata = (key) => {
    delete this.metadata[String(key)];
    return this;
  };

  _messageFormat = (type, message) => {
    return {
      type,
      message,
      meta: this.metadata,
    };
  };

  log = (type, message) => {
    this.winston.log({
      level: 'info',
      message: this._messageFormat(type, message),
    });
  };

  warn = (type, message) => {
    this.winston.log({
      level: 'warn',
      message: this._messageFormat(type, message),
    });
  };

  error = (type, message) => {
    this.winston.log({
      level: 'error',
      message: this._messageFormat(type, message),
    });
  };

  debug = (type, message) => {
    if (this.debugMode) {
      this.winston.log({
        level: 'info',
        message: this._messageFormat(type, message),
      });
    }
  };

  err = this.error;

  getLogger = () => this.winston;
}

module.exports = Logger;
